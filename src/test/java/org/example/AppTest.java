package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AppTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{}, "No arguments provided.", new int[]{}},
                {new String[]{"5"}, "Sorted numbers:\n5", new int[]{5}},
                {new String[]{"3", "1", "2"}, "Sorted numbers:\n1\n2\n3", new int[]{1, 2, 3}},
                {new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"},
                        "Sorted numbers:\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10", new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new String[]{"2", "4", "6", "8", "10", "12", "14", "16", "18", "20", "22"},
                        "Sorted numbers:\n2\n4\n6\n8\n10\n12\n14\n16\n18\n20\n22", new int[]{2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22}}
        });
    }

    private String[] inputArgs;
    private String expectedOutput;
    private int[] expectedSortedNumbers;

    public AppTest(String[] inputArgs, String expectedOutput, int[] expectedSortedNumbers) {
        this.inputArgs = inputArgs;
        this.expectedOutput = expectedOutput;
        this.expectedSortedNumbers = expectedSortedNumbers;
    }

    @Test
    public void testAppWithArguments() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        App.main(inputArgs);
        String actualOutput = outputStream.toString().trim().replace("\r\n", "\n"); // Replace CRLF with LF

        System.setOut(originalOut);

        assertEquals(expectedOutput, actualOutput);

        int[] actualSortedNumbers = Arrays.stream(actualOutput.split("\\n"))
                .skip(1)
                .mapToInt(Integer::parseInt)
                .toArray();
        assertArrayEquals(expectedSortedNumbers, actualSortedNumbers);
    }
}



